#!/usr/bin/python


from lcd_.dh1602a import DH1602A
from temperature.asus_cpu import AsusCPU

import ASUS.GPIO as GPIO
import time
import subprocess

LCD_RS = 162  # 16
LCD_E = 163  # 18

LCD_D4 = 188  # 40
LCD_D5 = 187  # 38
LCD_D6 = 223  # 36
LCD_D7 = 239  # 32

PWM_PIN = 238
TEMP_RANGE = [40, 80]
FAN = 0

lcdd = DH1602A(LCD_RS, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7)
AsusCPU = AsusCPU()

GET_IP_CMD ="hostname -I"

def main():
    lcdd.home()
    time.sleep(2)
    
    GPIO.setup(PWM_PIN, GPIO.OUT, initial=GPIO.LOW)
    pwm = GPIO.PWM(PWM_PIN, 25)
    pwm.start(0)

    while (True):
        data = AsusCPU.get()
        ip = run_cmd(GET_IP_CMD)
        fan = checkCoolingSys(pwm, awerageTemperature(data))
        mess = [
            "CPU Temp: {0:2.2f}".format(awerageTemperature(data)),
            "IP: " + ip,
            "FAN: " + str(fan)
        ]
        print(mess)
        lcdd.setCursor(1, 1)
        lcdd.printToLcd(mess[0])
        lcdd.setCursor(1, 2)
        lcdd.printToLcd(mess[1])

        time.sleep(2)


def run_cmd(cmd):
    return subprocess.check_output(cmd, shell=True).decode('utf-8')

def awerageTemperature(data):
    return sum(data) / len(data)

def checkCoolingSys(pwm, awTemp):
    maxVal = TEMP_RANGE[-1] - TEMP_RANGE[0]
    global FAN
    if (awTemp - TEMP_RANGE[0] > 0 ):
        fan = int(round((awTemp - TEMP_RANGE[0]) * 100 / maxVal))
        fan = 100 if fan > 100 else fan
        fan = 0 if fan < 20 else fan
        fan = 30 if FAN == 0 and fan != 0 else fan
        FAN = fan
        pwm.ChangeDutyCycle(fan)
        return fan
    else:
        FAN = 0
        pwm.ChangeDutyCycle(0)
        return 0
        

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
    finally:
        lcdd.clear()
        GPIO.cleanup()
