#!/usr/bin/python

import ASUS.GPIO as GPIO
import time

class DH1602A:
    LCD_CLEARDISPLAY = 0x01
    LCD_RETURNHOME = 0x02
    LCD_ENTRYMODESET = 0x04
    LCD_DISPLAYCONTROL = 0x08
    LCD_CURSORSHIFT = 0x10
    LCD_FUNCTIONSET = 0x20
    LCD_SETCGRAMADDR = 0x40
    LCD_SETDDRAMADDR = 0x80
    
    # flags for display entry mode
    LCD_ENTRYRIGHT = 0x00
    LCD_ENTRYLEFT = 0x02
    LCD_ENTRYSHIFTINCREMENT = 0x01
    LCD_ENTRYSHIFTDECREMENT = 0x00

    # flags for display on/off control
    LCD_DISPLAYON = 0x04
    LCD_DISPLAYOFF = 0x00
    LCD_CURSORON = 0x02
    LCD_CURSOROFF = 0x00
    LCD_BLINKON = 0x01
    LCD_BLINKOFF = 0x00

    # flags for display/cursor shift
    LCD_DISPLAYMOVE = 0x08
    LCD_CURSORMOVE = 0x00
    LCD_MOVERIGHT = 0x04
    LCD_MOVELEFT = 0x00

    # flags for function set
    LCD_8BITMODE = 0x10
    LCD_4BITMODE = 0x00
    LCD_2LINE = 0x08
    LCD_1LINE = 0x00
    # dot sizes
    LCD_5x10DOTS = 0x04
    LCD_5x8DOTS = 0x00
    
    LCD_WIDTH = 0x20
    
    _row_offsets = []
    _dotSize = 0x00
    
    columns = 16
    row = 2

    def __init__(self, pin_rs, pin_enable, d4, d5, d6, d7):
        self._rs = pin_rs
        self._enable = pin_enable
        self._d4 = d4
        self._d5 = d5
        self._d6 = d6
        self._d7 = d7
                                
        self._displayfunction = self.LCD_4BITMODE | self.LCD_2LINE | self.LCD_5x8DOTS 
        self._displaycontrol = self.LCD_DISPLAYON | self.LCD_CURSOROFF | self.LCD_BLINKOFF       
        self._displaymode = self.LCD_ENTRYLEFT | self.LCD_ENTRYSHIFTDECREMENT
        
        self._dotSize = self.LCD_5x8DOTS
        self.begin(16, 2)
        
    def gpio_init(self):
        print("GPIO INIT")
        GPIO.setmode(GPIO.ASUS)
        
        GPIO.setup(self._enable, GPIO.OUT)
        GPIO.setup(self._rs, GPIO.OUT)
        
        GPIO.setup(self._d4, GPIO.OUT)
        GPIO.setup(self._d5, GPIO.OUT)
        GPIO.setup(self._d6, GPIO.OUT)
        GPIO.setup(self._d7, GPIO.OUT)
        
    def gpioToLow(self, onlyData):
        if( (onlyData == False) ):
            GPIO.output(self._enable, False)  # E
            GPIO.output(self._rs, False) # RS
        GPIO.output(self._d4, False)
        GPIO.output(self._d5, False)            
        GPIO.output(self._d6, False)
        GPIO.output(self._d7, False)
        
        
    def begin(self, cols, lines):
        self.columns = 0
        self.rows = 0

        if (lines == 1):
            self._displayfunction |= self.LCD_2LINE;
        self._numlines = lines
        self.setRowOffsets(0x00, 0x40, 0x14, 0x54) 
        
        if ( (self._dotSize != self.LCD_5x8DOTS) and (line == 1) ):
            self._displayfunction |= self.LCD_5x10DOTS
        
        self.gpio_init()

        #self.gpioToLow(False)
        
        self.command(0x33)    
        self.command(0x32)        
        
        self.command(self.LCD_DISPLAYCONTROL | self._displaycontrol)
        self.command(self.LCD_FUNCTIONSET    | self._displayfunction)
        self.command(self.LCD_ENTRYMODESET   | self._displaymode )
        self.clear()
                    
    def clear(self):
        self.command(self.LCD_CLEARDISPLAY)
        time.sleep(0.003)        

    def home(self):
        self.command(self.LCD_RETURNHOME)
        time.sleep(0.003)

    def setCursor(self, col, row):
        command = self.LCD_SETDDRAMADDR | ((col-1) + self._row_offsets[row-1]);
        self.command(command)

    def send(self, value, mode):
        time.sleep(0.001)
        GPIO.output(self._rs, mode)
        GPIO.output(self._d4, ((value >> 4) & 1) > 0)
        GPIO.output(self._d5, ((value >> 5) & 1) > 0)            
        GPIO.output(self._d6, ((value >> 6) & 1) > 0)
        GPIO.output(self._d7, ((value >> 7) & 1) > 0)
        self.pulseEnable()
        GPIO.output(self._d4, (value & 1) > 0)
        GPIO.output(self._d5, ((value >> 1) & 1) > 0)            
        GPIO.output(self._d6, ((value >> 2) & 1) > 0)
        GPIO.output(self._d7, ((value >> 3) & 1) > 0)
        self.pulseEnable()
    
    def display(self):
        self.command(self.LCD_DISPLAYON ==  self.LCD_DISPLAYON & self._displaycontrol)
    
    def noDisplay(self):
        self._displaycontrol &= ~self.LCD_DISPLAYON
        self.command(self.LCD_DISPLAYCONTROL | self._displaycontrol)
    
    def message(self, message):
        self._message = message
        line = 0
        # Track times through iteration, to act on the initial character of the message
        initial_character = 0
        # iterate through each character
        for character in message:
            # If this is the first character in the string:
            if initial_character == 0:
                # Start at (1, 1) unless direction is set right to left, in which case start
                # on the opposite side of the display.
                col = 0 if self.displaymode & self.LCD_ENTRYLEFT > 0 else self.columns - 1
                self.setCursor(col, line)
                initial_character += 1
            # If character is \n, go to next line
            if character == '\n':
                line += 1
                # Start the second line at (1, 1) unless direction is set right to left in which
                # case start on the opposite side of the display.
                col = 0 if self.displaymode & self.LCD_ENTRYLEFT > 0 else self.columns - 1
                self.cursor_position(col, line)
            # Write string to display
            else:
                self.send(ord(character), True)
  
    def cursor(self):
        self._displaycontrol |= self.LCD_CURSORON
        self.command(self.LCD_DISPLAYCONTROL | self._displaycontrol)

    def noCursor(self):
        self._displaycontrol &= ~self.LCD_CURSORON
        self.command(self.LCD_DISPLAYCONTROL | self._displaycontrol)
    
    def blink(self):
        self._displaycontrol |= self.LCD_BLINKON
        self.command(self.LCD_DISPLAYCONTROL | self._displaycontrol)

    def noBlink(self):
        self._displaycontrol &= ~self.LCD_BLINKON
        self.command(self.LCD_DISPLAYCONTROL | self._displaycontrol)
        
    def scrollDisplayLeft(self):
        self.command(self.LCD_CURSORSHIFT | self.LCD_DISPLAYMOVE | self.LCD_MOVELEFT)

    def scrollDisplayRight(self):
        self.command(self.LCD_CURSORSHIFT | self.LCD_DISPLAYMOVE | self.LCD_MOVERIGHT)
   
    def leftToRight(self):
        self._displaymode |= self.LCD_ENTRYLEFT
        self.command(self.LCD_ENTRYMODESET | self._displaymode)

    def rightToLeft(self):
        self._displaymode &= ~self.LCD_ENTRYLEFT
        self.command(self.LCD_ENTRYMODESET | self._displaymode)
                
    def autoscroll(self):
        self._displaymode |= self.LCD_ENTRYSHIFTINCREMENT
        self.command(self.LCD_ENTRYMODESET | self._displaymode)

    def noAutoscroll(self):
        self._displaymode &= ~self.LCD_ENTRYSHIFTINCREMENT
        self.command(self.LCD_ENTRYMODESET | self._displaymode)
    
    def createChar(self, location, charmap):
        location &= 0x7
        self.command(LCD_SETCGRAMADDR | (location << 3))
        for i in range(0, 7):
            self.write(charmap[i])
    
    def printToLcd(self, message):
        message = message.ljust(self.LCD_WIDTH," ")
        for i in range((self.LCD_WIDTH-1)):
            self.write(ord(message[i]))
        
    def command(self, value):
         self.send(value, False)
         
    def write(self, value):
         self.send(value, True)    
        
    def setRowOffsets(self, row0, row1, row2, row3):
        self._row_offsets = [row0, row1, row2, row3]
 
    def pulseEnable(self): 
        GPIO.output(self._enable, False)
        time.sleep(0.0000001)
        GPIO.output(self._enable, True)
        time.sleep(0.0000001) # enable pulse must be >450ns
        GPIO.output(self._enable, False)
        time.sleep(0.0000001)