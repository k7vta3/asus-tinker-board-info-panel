#!/usr/bin/python


CPU_CORE_0 = '/sys/class/thermal/thermal_zone0/temp'
CPU_CORE_1 = '/sys/class/thermal/thermal_zone1/temp'


class AsusCPU:

    def __init__(self):
        print('ASUS tonker CPU temp init')
        self._tempFiles = [CPU_CORE_0, CPU_CORE_1]

    def get(self):
        result = []
        for path in self._tempFiles:
            raw_data = self._get_cpu_temp(path)
            result.append(self._parse_temp(raw_data))
        return result

    def _get_cpu_temp(self, path):
        f = open(path, 'r')
        return f.readline()

    def _parse_temp(self, raw_data):
        return float(raw_data) / 1000
